﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace RestoreForwardCalibrationCs
{
    public static class ArrayExtensions
    {
        public static byte[] ToByteArray<T>(this T data)
        {
            byte[] bytes = new byte[Marshal.SizeOf(data)];
            GCHandle h = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            IntPtr pBuffer = h.AddrOfPinnedObject();
            Marshal.StructureToPtr(data, pBuffer, true);
            h.Free();
            return bytes;
        }
    }

    class RestoreForwardCalibration
    {
        const byte STB_ERROR = 4;

        static byte ReadStatusByte(NationalInstruments.VisaNS.UsbSession session)
        {
            // Workaround
            // Sometimes you get an immediate timeout on ReadStatusByte from NI-VISA
            NationalInstruments.VisaNS.StatusByteFlags stb = 0;
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    stb = session.ReadStatusByte();
                    break;
                }
                catch (NationalInstruments.VisaNS.VisaException ex)
                {
                    if ((i < 5) && ex.ErrorCode == NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout)
                    {
                        // Try again
                        continue;
                    }
                    // Otherwise the exception is raised
                    throw ex;
                }
            }
            return (byte)stb;
        }

        static void CheckForErrors(NationalInstruments.VisaNS.UsbSession session)
        {
            // Check for error
            var stb = ReadStatusByte(session);
            if ((stb & STB_ERROR) == 0)
            {
                // No errors
                return;
            }

            for (;;)
            {
                // Get error count
                session.Write("SYST:ERR:COUN?");
                var response = session.ReadString();
                var count = Int32.Parse(response.Trim());
                if (count == 0)
                {
                    // No more errors to show
                    return;
                }

                // Get and display error message
                session.Write("SYST:ERR?");
                response = session.ReadString();
                Console.WriteLine(response.Trim());
            }
        }

        static void WaitForOperationComplete(NationalInstruments.VisaNS.UsbSession session)
        {
            session.Write("*OPC?");
            var response = session.ReadString().Trim('\n');
            if (response != "1")
            {
                Console.WriteLine(
                    "Oops! Expecting to receive \"1\" but got \"{0}\"\n" +
                    "Responses must be out of sync with requests.",
                    response);
            }
        }

        static byte[] EncodeBlockData(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                var length = string.Format("{0}", data.Length);
                var header = Encoding.ASCII.GetBytes(string.Format("#{0}{1}", length.Length, length));
                ms.Write(header, 0, header.Length);
                ms.Write(data, 0, data.Length);
                byte[] terminator = { 0x0a };
                ms.Write(terminator, 0, terminator.Length);
                return ms.ToArray();
            }
        }

        struct CalPoint
        {
            public CalPoint(double frequencyMHz, double wattsPerCount_HighRange, double wattsPerCount_LowRange)
            {
                this.FrequencyMHz = (float)frequencyMHz;
                this.WattsPerCount_HighRange = (float)wattsPerCount_HighRange;
                this.WattsPerCount_LowRange = (float)wattsPerCount_LowRange;
            }

            public float FrequencyMHz;
            public float WattsPerCount_HighRange;
            public float WattsPerCount_LowRange;
        }

        static readonly IList<CalPoint> CalPoints = new ReadOnlyCollection<CalPoint>
            (new[]
            {
                new CalPoint(1,     0.0005211663, 5.296073E-05),
                new CalPoint(1.135, 0.0005206466, 5.291668E-05),
                new CalPoint(1.495, 0.0005206375, 5.294355E-05),
                new CalPoint(1.63,  0.0005198108, 5.287362E-05),
                new CalPoint(2.53,  0.0005195891, 5.29887E-05),
                new CalPoint(2.845, 0.0005200389, 5.302935E-05),
                new CalPoint(3.295, 0.0005176489, 5.292956E-05),
                new CalPoint(4,     0.0005125962, 5.282405E-05),
                new CalPoint(4.5,   0.0005224803, 5.313246E-05),
                new CalPoint(5,     0.0005258647, 5.326205E-05),
                new CalPoint(6,     0.0005291431, 5.355098E-05),
                new CalPoint(8,     0.0005345751, 5.404161E-05),
                new CalPoint(9.91,  0.000542429,  5.476351E-05),
                new CalPoint(10,    0.0005431907, 5.480111E-05)
            });

        static void Main(string[] args)
        {
            var rm = NationalInstruments.VisaNS.ResourceManager.GetLocalManager();
            // Find all 4022 USBTMC sensors
            var resourceNames = rm.FindResources("USB0::0x1422::0x4022::?*::INSTR");
            // Open the first 4022 sensor found
            var session = (NationalInstruments.VisaNS.UsbSession)rm.Open(resourceNames[0]);

            Console.WriteLine("Restoring calibration to " + session.ResourceName);

            // Clear the sensor
            session.Write("*CLS");
            System.Threading.Thread.Sleep(500);

            // Unlock the sensor
            // This only works with 3% sensors. The 1% sensors cannot be unlocked.
            session.Write("FACT:USER:UNL \"k8DjFl2nOvJWrW0g\"");
            CheckForErrors(session);

            // Get the number of forward calibration points
            session.Write("FACT:CAL:FORW:NPO?");
            var response = session.ReadString();
            var count = Int32.Parse(response.Trim());

            // Delete each calibration point
            for (int i = count; i >= 1; i--)
            {
                // Specify the index in the calibration table
                session.Write(string.Format("FACT:CAL:POIN {0}", i));
                CheckForErrors(session);
                // Delete the calibration point
                session.Write("FACT:CAL:FORW:DEL");
                WaitForOperationComplete(session);
                CheckForErrors(session);
            }

            // Send the calibration points
            foreach (var cal_point in CalPoints)
            {
                var block_data = EncodeBlockData(cal_point.ToByteArray());
                var command = Encoding.ASCII.GetBytes("FACT:CAL:FORW:DATA ");
                session.Write(command.Concat(block_data).ToArray());
                WaitForOperationComplete(session);
                CheckForErrors(session);
            }

            // Commit the calibration
            session.Write("FACT:CAL:COMM");
            WaitForOperationComplete(session);
            CheckForErrors(session);
        }
    }
}
