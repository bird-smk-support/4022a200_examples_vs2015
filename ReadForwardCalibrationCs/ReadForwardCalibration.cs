﻿using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace ReadForwardCalibrationCs
{
    public static class ArrayExtensions
    {
        public static T ToStruct<T>(this byte[] data) where T : struct
        {
            T result = new T();
            int bufferSize = Marshal.SizeOf(typeof(T));
            IntPtr handle = Marshal.AllocHGlobal(bufferSize);
            Marshal.Copy(data, 0, handle, bufferSize);
            result = Marshal.PtrToStructure<T>(handle);
            Marshal.FreeHGlobal(handle);
            return result;
        }
    }

    class ReadForwardCalibration
    {
        const byte STB_ERROR = 4;

        static byte ReadStatusByte(NationalInstruments.VisaNS.UsbSession session)
        {
            // Workaround
            // Sometimes you get an immediate timeout on ReadStatusByte from NI-VISA
            NationalInstruments.VisaNS.StatusByteFlags stb = 0;
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    stb = session.ReadStatusByte();
                    break;
                }
                catch (NationalInstruments.VisaNS.VisaException ex)
                {
                    if ((i < 5) && ex.ErrorCode == NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout)
                    {
                        // Try again
                        continue;   
                    }
                    // Otherwise the exception is raised
                    throw ex;
                }
            }
            return (byte)stb;
        }

        static void CheckForErrors(NationalInstruments.VisaNS.UsbSession session)
        {
            // Check for error
            var stb = ReadStatusByte(session);
            if ((stb & STB_ERROR) == 0)
            {
                // No errors
                return;
            }

            for (;;)
            {
                // Get error count
                session.Write("SYST:ERR:COUN?");
                var response = session.ReadString();
                var count = Int32.Parse(response.Trim());
                if (count == 0)
                {
                    // No more errors to show
                    return;
                }

                // Get and display error message
                session.Write("SYST:ERR?");
                response = session.ReadString();
                Console.WriteLine(response.Trim());
            }
        }

        static byte[] ParseBlockData(byte[] block_data)
        {
            if (Convert.ToChar(block_data[0]) != '#')
            {
                throw new Exception("Block data missing leading '#'.");
            }
            var s = System.Text.Encoding.ASCII.GetString(block_data.Skip(1).Take(1).ToArray());
            var length_of_data_length = Convert.ToInt32(s);
            s = System.Text.Encoding.ASCII.GetString(block_data.Skip(2).Take(length_of_data_length).ToArray());
            var data_length = Convert.ToInt32(s);

            if (block_data.Length < (2 + length_of_data_length + data_length))
            {
                throw new Exception("Block data received less than expected.");
            }
            return block_data.Skip(length_of_data_length + 2).ToArray();
        }

        struct CalPoint
        {
            public float FrequencyMHz;
            public float WattsPerCount_HighRange;
            public float WattsPerCount_LowRange;
        }


        static void Main(string[] args)
        {
            var rm = NationalInstruments.VisaNS.ResourceManager.GetLocalManager();
            // Find all 4022 USBTMC sensors
            var resourceNames = rm.FindResources("USB0::0x1422::0x4022::?*::INSTR");
            // Open the first 4022 sensor found
            var session = (NationalInstruments.VisaNS.UsbSession)rm.Open(resourceNames[0]);

            // Clear the sensor
            session.Write("*CLS");
            System.Threading.Thread.Sleep(500);

            // Unlock the sensor
            // This only works with 3% sensors. The 1% sensors cannot be unlocked.
            session.Write("FACT:USER:UNL \"k8DjFl2nOvJWrW0g\"");
            CheckForErrors(session);

            // Get the number of forward calibration points
            session.Write("FACT:CAL:FORW:NPO?");
            var response = session.ReadString();
            var count = Int32.Parse(response.Trim());

            Console.WriteLine("From " + session.ResourceName);
            Console.WriteLine("Forward Calibration:");
            Console.WriteLine("MHz, Watts/Count (High Range), Watts/Count (Low Range)");

            // Read and display each calibration point
            for (int i = 1; i <= count; i++)
            {
                // Specify the index in the calibration table
                session.Write(string.Format("FACT:CAL:POIN {0}", i));
                CheckForErrors(session);
                // Request the calibration point
                session.Write("FACT:CAL:FORW:DATA?");
                // Block data is returned
                var block_data = session.ReadByteArray();
                var raw_data = ParseBlockData(block_data);
                var cal_point = raw_data.ToStruct<CalPoint>();
                Console.WriteLine("{0}, {1}, {2}", cal_point.FrequencyMHz, cal_point.WattsPerCount_HighRange, cal_point.WattsPerCount_LowRange);
            }
        }
    }
}
