﻿Imports System.Runtime.InteropServices

Module ReadForwardCalibration
    Const STB_ERROR = 4

    Function ReadStatusByte(session As NationalInstruments.VisaNS.UsbSession)
        ' Workaround
        ' Sometimes you get an immediate timeout on ReadStatusByte from NI-VISA
        Dim stb As Byte
        For i = 1 To 5
            Try
                stb = session.ReadStatusByte()
                Exit For
            Catch ex As NationalInstruments.VisaNS.VisaException When i < 5 And ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout
                ' Try again
                Continue For
                ' Otherwise the exception is raised
            End Try
        Next i
        Return stb
    End Function

    Sub CheckForErrors(session As NationalInstruments.VisaNS.UsbSession)
        ' Check for error
        Dim stb As Byte = ReadStatusByte(session)
        If (stb And STB_ERROR) = 0 Then
            ' No errors
            Return
        End If

        Do
            ' Get error count
            session.Write("SYST:ERR:COUN?")
            Dim response As String = session.ReadString()
            Dim count As Int32 = Convert.ToInt32(response.Trim())
            If count = 0 Then
                ' No more errors to show
                Return
            End If

            ' Get and display error message
            session.Write("SYST:ERR?")
            response = session.ReadString()
            Console.WriteLine(response.Trim())
        Loop
    End Sub

    Function ParseBlockData(block_data As Byte()) As Byte()
        If Convert.ToChar(block_data(0)) <> "#" Then
            Throw New Exception("Block data missing leading '#'.")
        End If
        Dim s As String = System.Text.Encoding.ASCII.GetString(block_data.Skip(1).Take(1).ToArray())
        Dim length_of_data_length As Int32 = Convert.ToInt32(s)
        s = System.Text.Encoding.ASCII.GetString(block_data.Skip(2).Take(length_of_data_length).ToArray())
        Dim data_length As Int32 = Convert.ToInt32(s)
        If block_data.Length < 2 + length_of_data_length + data_length Then
            Throw New Exception("Block data received less than expected.")
        End If
        Return block_data.Skip(length_of_data_length + 2).ToArray()
    End Function

    Function ByteArrayToStructure(ByVal data() As Byte, ByVal structure_type As System.Type) As Object
        Dim GC As GCHandle = GCHandle.Alloc(data, GCHandleType.Pinned)
        Dim Obj As Object = Marshal.PtrToStructure(GC.AddrOfPinnedObject, structure_type)
        Return Obj
        GC.Free()
    End Function

    Structure CalPoint
        Dim FrequencyMHz As Single
        Dim WattsPerCount_HighRange As Single
        Dim WattsPerCount_LowRange As Single
    End Structure

    Sub Main()
        Dim rm = NationalInstruments.VisaNS.ResourceManager.GetLocalManager()
        Dim resourceNames As String()
        Dim session As NationalInstruments.VisaNS.UsbSession
        Dim response As String
        Dim count As Int32
        Dim block_data As Byte()
        Dim raw_data As Byte()
        Dim cal_point As CalPoint

        ' Find all 4022 USBTMC sensors
        resourceNames = rm.FindResources("USB0::0x1422::0x4022::?*::INSTR")
        ' Open the first 4022 sensor found
        session = rm.Open(resourceNames(0))

        ' Clear the sensor
        session.Write("*CLS")
        Threading.Thread.Sleep(500)

        ' Unlock the sensor
        ' This only works with 3% sensors. The 1% sensors cannot be unlocked.
        session.Write("FACT:USER:UNL ""k8DjFl2nOvJWrW0g""")
        CheckForErrors(session)

        ' Get the number of forward calibration points
        session.Write("FACT:CAL:FORW:NPO?")
        response = session.ReadString()
        count = Convert.ToInt32(response.Trim())

        Console.WriteLine("From " + session.ResourceName)
        Console.WriteLine("Forward Calibration:")
        Console.WriteLine("MHz, Watts/Count (High Range), Watts/Count (Low Range)")

        ' Read and display each calibration point
        For i = 1 To count
            ' Specify the index in the calibration table
            session.Write("FACT:CAL:POIN " + Convert.ToString(i))
            CheckForErrors(session)
            ' Request the calibration point
            session.Write("FACT:CAL:FORW:DATA?")
            ' Block data is returned
            block_data = session.ReadByteArray()
            raw_data = ParseBlockData(block_data)
            cal_point = ByteArrayToStructure(raw_data, cal_point.GetType)
            Console.WriteLine("{0}, {1}, {2}", cal_point.FrequencyMHz, cal_point.WattsPerCount_HighRange, cal_point.WattsPerCount_LowRange)
        Next i
    End Sub

End Module
