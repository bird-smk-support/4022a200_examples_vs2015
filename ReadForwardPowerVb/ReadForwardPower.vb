﻿Module ReadForwardPower

    Sub Main()
        Dim rm = NationalInstruments.VisaNS.ResourceManager.GetLocalManager()
        Dim resourceNames As String()
        Dim session As NationalInstruments.VisaNS.UsbSession
        Dim result As String

        ' Find all 4022 USBTMC sensors
        resourceNames = rm.FindResources("USB0::0x1422::0x4022::?*::INSTR")
        ' Open the first 4022 sensor found
        session = rm.Open(resourceNames(0))

        ' Get and display the forward power
        session.Write("FETC:AVER?")
        result = session.ReadString()
        Console.WriteLine("From " + session.ResourceName + " Forward power = " + result.Trim() + " watts")
    End Sub

End Module
