Projects in this solution are dependent on a VISA implementation such as NI-VISA which can be obtained here:
https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html.

It may be necessary to remove the reference to NationalInstruments.VisaNS and replace it with a reference to 
whatever VISA implementation you are using.